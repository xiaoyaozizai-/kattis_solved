import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Simon{
    public static void main(String[] args) {
        int i;
        int k;
        int cases = 0;
        String line;

        String[] tokens;

        BufferedReader br;

        br = new BufferedReader(new InputStreamReader(System.in));

        try {
            line = br.readLine();
            cases = Integer.parseInt(line);

            for (i = 0; i < cases ; i++) {
                line = br.readLine();
                tokens = line.split(" ");

                if(tokens.length > 1 && tokens[0].equals("simon") && tokens[1].equals("says")) {
                    for (k = 2; k < tokens.length ; k++) {
                        System.out.print(tokens[k] + " ");  
                    }
                }
                
                System.out.println();
            }

            br.close();
        }catch (IOException e){
            throw new RuntimeException(e);
        }
    }
}