import java.util.Scanner;
import java.util.Arrays;

public class Server {
    public static void main(String[] args) {
        Solve();
    }
    
    public static void Solve() {
        int sum = 0;
        int timeLimit;
        int numOfTasks;
        int numOfTasksDone = 0;
        
        String[] details;
        String[] taskString;
        Integer[] tasksTimeArray;
        
        Scanner scan = new Scanner(System.in);
        
        details = scan.nextLine().split(" ");
        taskString = scan.nextLine().split(" ");
        
        numOfTasks = Integer.parseInt(details[0]);
        timeLimit = Integer.parseInt(details[1]);
        
        tasksTimeArray = new Integer[taskString.length];
        
        for(int i = 0; i < taskString.length; i++) {
            tasksTimeArray[i] = Integer.parseInt(taskString[i]);
        }
        
        for(int i = 0; i < tasksTimeArray.length; i++) {
            sum = sum + tasksTimeArray[i];
            
            if(sum <= timeLimit) {
                numOfTasksDone++;
            } else {
                break;
            }
        }
        
        System.out.println(numOfTasksDone);
    }
}