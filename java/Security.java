/*  
    Author: Brian Joshua M. Revilla
    SN    : 2012-69906
*/

import java.util.Map;
import java.util.Scanner;
import java.util.HashMap;
import java.util.ArrayList;
import java.lang.StringBuilder;

public class Security {

    public static void main (String[] args) {
        int indexStart;
        int indexEnd;
        int codeLength;
        String line;
        String morseLine;
        String lengthString;
        String resultString;
        Scanner sc;
        HashMap<Character, String> map;
        HashMap<String, Character> map2;
        StringBuilder morseSB;
        StringBuilder lengthSB;
        StringBuilder resultSB;
        
        ArrayList<String> results = new ArrayList<String>();
        
        /* Create a hashtable for the char->morse code */
        map = new HashMap<Character, String>(41);
        map2 = new HashMap<String, Character>(41);
        
        map.put('A', ".-");
        map.put('B', "-...");
        map.put('C', "-.-.");
        map.put('D', "-..");
        map.put('E', ".");
        map.put('F', "..-.");
        map.put('G', "--.");
        map.put('H', "....");
        map.put('I', "..");
        map.put('J', ".---");
        map.put('K', "-.-");
        map.put('L', ".-..");
        map.put('M', "--");
        map.put('N', "-.");
        map.put('O', "---");
        map.put('P', ".--.");
        map.put('Q', "--.-");
        map.put('R', ".-.");
        map.put('S', "...");
        map.put('T', "-");
        map.put('U', "..-");
        map.put('V', "...-");
        map.put('W', ".--");
        map.put('X', "-..-");
        map.put('Y', "-.--");
        map.put('Z', "--..");
        map.put('_', "..--");
        map.put(',', ".-.-");
        map.put('.', "---.");
        map.put('?', "----");
        
        map2.put(".-", 'A');
        map2.put("-...", 'B');
        map2.put("-.-.", 'C');
        map2.put("-..", 'D');
        map2.put(".", 'E');
        map2.put("..-.", 'F');
        map2.put("--.", 'G');
        map2.put("....", 'H');
        map2.put("..", 'I');
        map2.put(".---", 'J');
        map2.put("-.-", 'K');
        map2.put(".-..", 'L');
        map2.put("--", 'M');
        map2.put("-.", 'N');
        map2.put("---", 'O');
        map2.put(".--.", 'P');
        map2.put("--.-", 'Q');
        map2.put(".-.", 'R');
        map2.put("...", 'S');
        map2.put("-", 'T');
        map2.put("..-", 'U');
        map2.put("...-", 'V');
        map2.put(".--", 'W');
        map2.put( "-..-", 'X');
        map2.put( "-.--", 'Y');
        map2.put( "--..", 'Z');
        map2.put( "..--", '_');
        map2.put( ".-.-", ',');
        map2.put( "---.", '.');
        map2.put( "----", '?');
        
        sc = new Scanner(System.in);
        
        while(sc.hasNextLine() && !(line = sc.nextLine()).equals("")) {
            morseLine = "";
            lengthString = "";
            resultString = "";
            indexStart = 0;
            indexEnd = 0;
            
            lengthSB = new StringBuilder(lengthString);
            morseSB = new StringBuilder(morseLine);
            
            for(int i = 0; i < line.length(); i++) {
                if(map.containsKey(line.charAt(i))) {
                    morseSB.append(map.get(line.charAt(i)));
                    lengthSB.append(map.get(line.charAt(i)).length());
                } else {
                    System.exit(0);             
                }
            }
            
            //Reverse lengthSB String
            lengthSB.reverse();
            lengthSB.toString();

            resultSB = new StringBuilder(resultString);
            
            for(int i = 0; i < lengthSB.length(); i++) {
                codeLength = Character.getNumericValue(lengthSB.charAt(i));
                if(map2.containsKey(morseSB.substring(indexStart, indexEnd + codeLength))) {
                    resultSB.append(map2.get(morseSB.substring(indexStart, indexEnd + codeLength)));
                    indexStart += codeLength;
                    indexEnd += codeLength;
                } else {
                    System.exit(0);
                }
            }
            
            results.add(resultSB.toString());
        }
        
        for(String x: results) {
            System.out.println(x);
        }
        
        sc.close();
    }
}