/*
    Author: Brian Joshua M. Revilla
    SN    : 2012-69906

*/

import java.util.Scanner;
import java.util.Arrays;
import java.util.Collections;
import java.lang.StringBuilder;

public class ClosingTheLoop {
    public static void main(String[] args) {
        int min;
        int redIndex;
        int blueIndex;
        int casesNum;
        int stringNum;
        int tempInt;
        int loopLength = 0;
        
        int[] loopLengthArray; 
        
        Scanner sc; 
        StringBuilder sb;
        
        sc = new Scanner(System.in);
        casesNum = sc.nextInt();
        
        loopLengthArray = new int[casesNum];
        
        for(int i = 0; i < casesNum; i++) {
            redIndex = 0;
            blueIndex = 0;
            loopLength = 0;
            
            stringNum = sc.nextInt();
            
            Integer[] red = new Integer[stringNum];
            Integer[] blue = new Integer[stringNum];
            
            Arrays.fill(red, 0);
            Arrays.fill(blue, 0);
            
            for(int j = 0; j < stringNum; j++) {
                sb = new StringBuilder(sc.next());
                
                if(sb.charAt(sb.length() - 1) == 'R') {
                    red[redIndex] = Integer.parseInt(sb.deleteCharAt(sb.length() - 1).toString());;
                    redIndex++;
                } else if(sb.charAt(sb.length() - 1) == 'B') {
                    blue[blueIndex] = Integer.parseInt(sb.deleteCharAt(sb.length() - 1).toString());;
                    blueIndex++;
                }   
            }
            
            Arrays.sort(red, Collections.reverseOrder());
            Arrays.sort(blue, Collections.reverseOrder());
            
            min = Math.min(redIndex,blueIndex);
            
            for(int k = 0; k < min; k++) {
                loopLength += red[k] + blue[k];
            }
            
            loopLengthArray[i] = loopLength - (min * 2);
        }
        
        for(int y = 1; y <= casesNum; y++) {
            System.out.println("Case #" + y + ": " + loopLengthArray[y-1]);
        }
        
        sc.close();
    }
}