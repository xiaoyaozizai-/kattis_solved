/*
    Author: Brian Joshua M. Revilla
    SN    : 2012-69906
*/

import java.util.HashMap;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;

public class RPS {
    public static void main(String[] args) {
        int p1;
        int p2;
        int playerNum;
        int roundNum;
        String line;
    
        BufferedReader br;
        HashMap<String, String> decision;
        
        int[] wins;
        int[] loss;
        String[] tokens;
        
        decision = new HashMap<String, String>(3);
        decision.put("rock", "scissors");
        decision.put("scissors", "paper");
        decision.put("paper", "rock");
        
        br = new BufferedReader(new InputStreamReader(System.in));
        
        try {
            while((line = br.readLine()) != null) {
                tokens = line.split(" ");
                playerNum = Integer.parseInt(tokens[0]);
            
                if(playerNum == 0) {
                    System.exit(0);
                }
            
                roundNum = Integer.parseInt(tokens[1]);
            
                wins = new int[playerNum];
                loss = new int[playerNum];
            
                for(int i = 0; i < (roundNum * playerNum * (playerNum - 1))/2; i++) {
                    tokens = br.readLine().split(" ");
                
                    p1 = Integer.parseInt(tokens[0]);
                    p2 = Integer.parseInt(tokens[2]);
                
                    // Conditions
                    if(decision.get(tokens[1]).equals(tokens[3])) {
                        wins[p1 - 1]++;
                        loss[p2 - 1]++;
                    } else if(decision.get(tokens[3]).equals(tokens[1]))  {
                        wins[p2 - 1]++;
                        loss[p1 - 1]++;
                    }
                }
            
                System.out.println();
                for(int k = 0; k < playerNum; k++) {
                    if(wins[k] + loss[k] == 0) {    
                        System.out.println("-");
                    } else {
                        System.out.printf("%.3f", (float)wins[k]/(wins[k] + loss[k]));
                        System.out.println();
                    }
                }
            }
            
            br.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}