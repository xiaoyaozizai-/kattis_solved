include <stdio.h>

int main() {
    int temp;
    int hours;
    int minutes;
    int finalHours;
    int finalMinutes;
    
    scanf("%d %d", &hours, &minutes);
    
    /* Checking of inputs */
    if(hours < 0 || hours > 23 || minutes < 0 || minutes > 59) {
        return 0;
    }
    
    temp = minutes - 45;
    
    if(temp < 0) {
        finalMinutes = 60 - (temp * -1);
        finalHours = hours - 1;
        
        if(finalHours < 0) {
            finalHours = 23;
        }
    } else {
        finalMinutes = temp;
        finalHours = hours;
    }
    
    printf("%d %d\n", finalHours, finalMinutes);
    
    return 0;
}