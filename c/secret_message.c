#include <stdio.h>
#include <math.h>
#include <string.h>

#define MAXLENGTH 10000

int main() {
    int i;
    int j;
    int k;
    int l;
    int n;
    int cnt;
    int len;
    int matSize;
    
    char str[MAXLENGTH];
    
    scanf("%d", &n);
    
    for(i = 0; i < n; i++) {
        scanf("%s", str);
        
        len = (int)strlen(str);
        k = ceil(sqrt(len));
        
        int matrix[k][k];
        
        cnt = 0;
        for(j = 0; j < k; j++)
            for(l = 0; l < k; l++) {
                if(cnt < len) {
                    matrix[j][l] = str[cnt++];
                } else {
                    matrix[j][l] = '*';
                }       
            }
        
        for(j = 0; j < k; j++)
            for(l = k - 1; l >= 0; l--) {
                if(matrix[l][j] != '*')
                    printf("%c", matrix[l][j]);
            }
        
        printf("\n");
    }

    return 0;
}