#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STR_LEN 1000

int countInstances(char *string);

int main() {
    char inputString[MAX_STR_LEN];
    
    int i;
    int answer;
    int inputStrLen;
    
    scanf("%s", inputString);
    inputStrLen = strlen(inputString);
    
    /* Check if lower cases only and strlen < 1000 */
    if(inputStrLen > 0 && inputStrLen <= 1000) {
        for(i = 0; i < inputStrLen; i++) {
            if(!islower(inputString[i])) {
                return 0;
            }
        }
    }   
        
    answer = countInstances(inputString);
    printf("%d\n", answer);

    return 0;
}


int countInstances(char *string) {
    int j;
    int index;
    int oddFlag = 0;
    int numOfRemove = 0;

    //Create array that will represent the alphabets.
    int instances[25] = {0};
    
    //Count the number of times letters appear.
    for(j = 0; j < strlen(string); j++) {
        index = (string[j] - '0') - 49;
        instances[index]++;
    }
    
    /* Check Peragram Conditions
     *  - All occurence of each letters must be even or.
     *  - One of the letters has an odd number of occurence.
     */
     
     for(j = 0; j < 26; j++) {
        if(instances[j] != 0){
            if(instances[j] % 2 != 0) {
                if(oddFlag == 0) {
                    oddFlag = 1;
                } else {
                    numOfRemove++;
                } 
            }
        }       
     }
     
     return numOfRemove;
}