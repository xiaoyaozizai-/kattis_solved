#include <stdio.h>

#define PI 3.14159265358979323846

int main() {
    int m;
    int c;
    double r;
    double trueArea;
    double relativeArea;
    
    while(1) {
        scanf("%lf %d %d", &r, &m, &c);
        
        if(r == 0 && m == 0 && c == 0) {
            return 0;       
        }
        
        trueArea = PI * (r * r);
        relativeArea = (4 * r * r) * (double)c/m;
        
        printf("%lf %lf\n", trueArea, relativeArea);
    }   
}