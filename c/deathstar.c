#include <stdio.h>
#define MAXSIZE 1000

int main() {
    int i;
    int j;
    int n;
    int num;
    
    scanf("%d", &n);
    
    int answer[MAXSIZE] = {0};
    
    for(i = 0; i < n; i++) {
        for(j = 0; j < n; j++) {
            scanf("%d", &num);
            answer[i] |= num;
        }
    }
    
    for(i = 0; i < n; i++) {
        printf("%d ", answer[i]);
    }
    
    return 0;
}