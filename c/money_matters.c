/*
    Author: Brian Joshua M. Revilla
    SN    : 2012-69906
*/

#include <stdio.h>
#include <stdlib.h>

void depthFirst(int **adjMat, int visited[], int i, int n, int costs[]);

int flag = 0;
int sum = 0;

int main(int argc, char const *argv[]) {
    
    int **adjMatrix;
    
    int i;
    int j;
    int k;
    int numFriends;
    int connections;
    
    scanf("%d %d", &numFriends, &connections);
    
    int costs[numFriends];
    int visited[numFriends];
    
    adjMatrix = malloc(sizeof(int *) * numFriends);
    
    for(i = 0; i < numFriends; i++){
        visited[i] = 0;     
        adjMatrix[i] = malloc(sizeof(int) * numFriends);
    }
    
    for (i = 0; i < numFriends; i++) {
        scanf("%d", &costs[i]);
    }
    
    for(i = 0; i < numFriends; i++) {
        for(j = 0; j < numFriends; j++) {
            adjMatrix[i][j] = 0;
        }
    }
    
    for(i = 0; i < connections; i++) {
        scanf("%d %d", &j, &k);
        adjMatrix[j][k] = 1;
        adjMatrix[k][j] = 1;
    }
    
    // Start at 0;
    depthFirst(adjMatrix, visited, 0, numFriends, costs);
    
    for(i = 0; i < numFriends; i++) {
        if(visited[i] == 0) { 
            if(sum != 0) {
                flag = 1;
                break;
            } else {
                depthFirst(adjMatrix, visited, i, numFriends, costs);           
            }
            
        }
    }
    
    if(flag == 1) {
        printf("IMPOSSIBLE\n");
    } else {
        printf("POSSIBLE\n");
    }
    
    for(i = 0; i < numFriends; i++) {
        free(adjMatrix[i]);
    }
    
    free(adjMatrix);
}

void depthFirst(int **adjMat, int visited[], int i, int n, int debts[]) {
    int l;
    visited[i] = 1;
    sum += debts[i];
    
    for(l = 0; l < n; l++) {
        if(visited[l] == 0 && adjMat[i][l] == 1) {
            depthFirst(adjMat, visited, l, n, debts);
        }
    }
}
