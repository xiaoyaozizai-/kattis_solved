// bjmrevilla - 02/06/2018
// How Many Digits - https://open.kattis.com/problems/howmanydigits
// Solved using Stirling Approximation - https://brilliant.org/wiki/finding-digits-of-a-number/

#include <stdio.h>
#include <math.h>

int getExponent(int x);

int main() {
  long int i;
  long double x;

  const double logOfE  = 0.4342944819;
  const double halflog10Of2 = 0.150514997832;
  const double halflog10OfPI = 0.248574936347;

  while((scanf("%ld", &i)) != EOF){
      if(i < 0) printf("0\n"); 
      else if(i <= 3) printf("1\n");
      else {
        x = halflog10Of2 + halflog10OfPI + log10(sqrt(i)) + (i * log10(i)) - (i * logOfE);
        printf("%d\n", (int)(floor(x)) + 1);
      }
  } 
}