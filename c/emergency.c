#include <stdio.h>

int main() {
    long long int kMultiple;
    long long int numberOfNodes;
    long long int pathCounter;
    long long int numberOfMultiple;
    long long int greatestMultiple;
    
    scanf("%lli %lli", &numberOfNodes, &kMultiple);
    
    /* Check the inputs */
    if(numberOfNodes < 1 || kMultiple < 1 || numberOfNodes > 1000000000000000000 || kMultiple > 1000000000000000000) {
        return 0;
    }
    
    numberOfMultiple = (numberOfNodes - 1)/kMultiple;
    
    if(numberOfMultiple == 0 || numberOfMultiple == 1) {
        pathCounter = numberOfNodes - 1;
    } else {
        greatestMultiple = numberOfMultiple * kMultiple;
        
        /* Path from the last node to the greatest multiple of k */
        pathCounter = (numberOfNodes - 1) - greatestMultiple;
    
        /* Path from the start node to k */
        pathCounter = pathCounter + kMultiple;
        
        /* Add one for the path from k to greatest k Multiple */
        pathCounter++;
    }
    
    printf("%lli\n", pathCounter);
}