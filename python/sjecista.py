# bjmrevilla
# July 9, 2018
# Description: Consider a convex polygon with N vertices, with the additional property that no three diagonals intersect in a single point (Meaning that they are not regular polygon). Find the number of intersections between pairs of diagonals in such a polygon.

# (n(n-1)(n-2)(n-3))/24 -> The answer will be always less than or equal to the answer of this equation. Ref: https://math.stackexchange.com/questions/1010591/what-is-the-number-of-intersections-of-diagonals-in-a-convex-equilateral-polygon

x = int(input())

if not x < 3 and not x > 100: 
	num_of_diagonals = int((x * (x - 1) * (x - 2) * (x - 3))/24)
	print(num_of_diagonals)