import string

n = int(input())

for x in range(0, n): 
	inputStr = input().lower()
	inputStr = ''.join(c for c in inputStr if c.isalpha())
	missing = set(set(inputStr) ^ set(string.ascii_lowercase))

	if not missing: print('pangram')
	else: print('missing ' + ''.join(sorted(missing)))

