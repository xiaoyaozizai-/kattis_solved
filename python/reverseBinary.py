n = int(input())

binaryString = "{0:b}".format(n)
reversedString = binaryString[::-1]

print(int(reversedString, 2))