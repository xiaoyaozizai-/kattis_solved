from sys import stdin, exit

count = 1
for line in stdin: 
  if not line: sys.exit()
  tokens = line.split()
  intTokens = list(map(int, tokens[1:]))

  minList = min(intTokens)
  maxList = max(intTokens)
  rangeList = maxList - minList

  print("Case %d: %d %d %d" % (count, minList, maxList, rangeList))
  count += 1