# bjmrevilla - 07/11/2018
# Maximum Subarray Problem using Kadane's Algorithm. 

def kadane(A): 
	max_current = max_global = A[0]
	for i in range(1, len(A)): 
		max_current = max(A[i], max_current + A[i])
		if max_current > max_global: 
		  max_global = max_current
	return max_global

x = input()
n, p = map(int, x.split())

s = input()
s = map(int, s.split())
s = [a - p for a in s]

o = kadane(s)
print(o)