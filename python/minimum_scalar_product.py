# bjmrevilla - 07/11/2018
# Minimum Scalar Product. 

t = int(input())
ans = []

for i in range(0, t): 
	n = int(input())
	x = map(int, input().split())
	y = map(int, input().split())

	sortedX = sorted(x)
	sortedY = sorted(y, reverse=True)

	ans.append(sum(map(lambda j, k: j * k, sortedX, sortedY)))

for i in range(1, t + 1): 
	print('Case #%d: %d' % (i, ans[i - 1]))