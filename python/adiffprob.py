from sys import stdin, exit

for line in stdin: 
  if not line: sys.exit()
  tokens = line.split()
  diff = int(tokens[0]) - int(tokens[1])
  if diff < 0: diff *= -1

  print(diff)