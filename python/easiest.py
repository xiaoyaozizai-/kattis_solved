def getDigitSum(n): 
  s = 0
  while n: 
    s, n = s + n % 10, n // 10
  return s

while True: 
  n = int(input())
  if n == 0: break
  else: 
    sumN = getDigitSum(n)

    for x in range(11, 100000): 
      if getDigitSum(x * n) == sumN: 
        print(x)
        break;